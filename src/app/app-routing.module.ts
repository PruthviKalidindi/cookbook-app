import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/Recipes',
    pathMatch: 'full'
  },
  {
    path: 'Recipes',
    loadChildren: () => import('src/app/recipes/recipes.module').then(r => r.RecipesModule)
  },
  {
    path: 'Ingredients',
    loadChildren: () => import('src/app/ingredients/ingredients.module').then(r => r.IngredientsModule)
  },
  {
    path: 'Measurements',
    loadChildren: () => import('src/app/measurements/measurements.module').then(r => r.MeasurementsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
