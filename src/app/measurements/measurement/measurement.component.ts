import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { measurementDatasourceService } from 'src/app/shared/services/measurement-datasource.service';
import { MeasurementService } from 'src/app/shared/services/measurement.service';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-measurement',
  templateUrl: './measurement.component.html',
  styleUrls: ['./measurement.component.scss']
})
export class MeasurementComponent {
  constructor(private fb: FormBuilder,
    public measurementService: MeasurementService,
    public measurementDatasourceService: measurementDatasourceService,
    public notificationService: NotificationService,
    public dialogRef: MatDialogRef<MeasurementComponent>) {}

  onSubmit(): void {
    let operationMessage: string = !this.hasKey() ? "Created" : "Updated";
    if(this.measurementService.form.valid){
      if(!this.hasKey()){
        this.measurementService.createItem(this.measurementService.form.value).subscribe((response: any) => {
          this.measurementDatasourceService.refresh();
        });
      } else {
        this.measurementService.updateItem(this.measurementService.form.get('id')?.value, this.measurementService.form.value).subscribe((response: any) => {
          this.measurementDatasourceService.refresh();
        });
      }
      this.onClear();
      this.notificationService.onSuccess(`:: ${operationMessage} Successfully!`);
      this.onClose();
    }
  }

  onClear(){
    this.measurementService.form.reset();
    this.measurementService.initializeFormGroup();
    this.notificationService.onSuccess(':: Cleared Successfully!');
  }

  onClose(){
    this.dialogRef.close();
  }

  getToolbarTitle(){
    return this.hasKey() ? "Modify Measurement" : "New Measurement";
  }

  getUpsertButtonText(){
    return this.hasKey() ? "Update" : "Add";
  }

  hasKey(){
    return this.measurementService.form.get('id')?.value;
  }
}
