import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as _ from 'lodash';
import { Measurement } from 'src/app/shared/models/measurement';
import { measurementDatasourceService } from 'src/app/shared/services/measurement-datasource.service';
import { MeasurementService } from 'src/app/shared/services/measurement.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { MeasurementComponent } from '../measurement/measurement.component';

@Component({
  selector: 'app-measurement-list',
  templateUrl: './measurement-list.component.html',
  styleUrls: ['./measurement-list.component.scss']
})
export class MeasurementListComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  measurementData!: Measurement;
  displayedColumns: string[] = ['name', 'description', 'actions'];

  constructor(public dialog: MatDialog,
    public measurementService: MeasurementService,
    public measurementDatasourceService: measurementDatasourceService,
    public notificationService: NotificationService) {
    this.measurementData = {} as Measurement;
  }

  applyFilter(event: Event) {
    this.measurementDatasourceService.applyFilter(event);
  }

  ngOnInit(): void {
    this.measurementDatasourceService.init(this.sort, this.paginator);
    this.getAllMeasurements();
  }

  getAllMeasurements() {
    return this.measurementDatasourceService.getAllMeasurements();
  }

  editMeasurement(element: Measurement) {
    this.measurementData = _.cloneDeep(element);
    this.measurementService.populateForm(this.measurementData);
    this.openMeasurementDialog();
  }

  deleteMeasurement(id: number) {
    this.measurementDatasourceService.deleteMeasurement(id);
    this.notificationService.onWarn(':: Deleted Successfully!');
  }

  addMeasurement() {
    this.measurementDatasourceService.addMeasurement(this.measurementData);
  }

  updateMeasurement() {
    this.measurementService.updateItem(this.measurementData.id, this.measurementData).subscribe((response: any) => {
      this.measurementDatasourceService.refresh();
    });
  }

  onCreate(){
    this.measurementService.form.reset();
    this.measurementService.initializeFormGroup();
    this.openMeasurementDialog();
  }

  openMeasurementDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "25%";
    this.dialog.open(MeasurementComponent, dialogConfig);
  }
}
