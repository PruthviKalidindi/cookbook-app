import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeasurementListComponent } from './measurement-list/measurement-list.component';
import { MaterialModule } from '../material/material.module';
import { MeasurementsComponent } from '../measurements/measurements.component';
import { MeasurementsRoutingModule } from './measurements-routing.module';
import { MeasurementComponent } from './measurement/measurement.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [MeasurementListComponent, MeasurementsComponent, MeasurementComponent],
  imports: [
    CommonModule,
    MeasurementsRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [],
  entryComponents: []
})
export class MeasurementsModule { }
