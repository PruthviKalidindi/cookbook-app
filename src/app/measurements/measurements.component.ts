import { AfterViewInit, Component } from '@angular/core';

@Component({
  selector: 'app-measurements',
  templateUrl: './measurements.component.html',
  styleUrls: ['./measurements.component.scss']
})
export class MeasurementsComponent implements AfterViewInit {
  title: string = "Measurements";

  constructor() {
    
  }

  ngAfterViewInit(): void {
  }
}
