import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { MaterialModule } from '../material/material.module';
import { IngredientsComponent } from '../ingredients/ingredients.component';
import { IngredientsRoutingModule } from './ingredients-routing.module';
import { IngredientComponent } from './ingredient/ingredient.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [IngredientListComponent, IngredientsComponent, IngredientComponent],
  imports: [
    CommonModule,
    FormsModule,
    IngredientsRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [],
  entryComponents: []
})
export class IngredientsModule { }
