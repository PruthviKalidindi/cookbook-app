import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { IngredientDatasourceService } from 'src/app/shared/services/ingredient-datasource.service';
import { IngredientService } from 'src/app/shared/services/ingredient.service';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent {
  constructor(private fb: FormBuilder,
    public ingredientService: IngredientService,
    public ingredientDatasourceService: IngredientDatasourceService,
    public notificationService: NotificationService,
    public dialogRef: MatDialogRef<IngredientComponent>) {}

  onSubmit(): void {
    let operationMessage: string = !this.hasKey() ? "Created" : "Updated";
    if(this.ingredientService.form.valid){
      if(!this.hasKey()){
        this.ingredientService.createItem(this.ingredientService.form.value).subscribe((response: any) => {
          this.ingredientDatasourceService.refresh();
        });
      } else {
        this.ingredientService.updateItem(this.ingredientService.form.get('id')?.value, this.ingredientService.form.value).subscribe((response: any) => {
          this.ingredientDatasourceService.refresh();
        });
      }
      this.onClear();
      this.notificationService.onSuccess(`:: ${operationMessage} Successfully!`);
      this.onClose();
    }
  }

  onClear(){
    this.ingredientService.form.reset();
    this.ingredientService.initializeFormGroup();
    this.notificationService.onSuccess(':: Cleared Successfully!');
  }

  onClose(){
    this.dialogRef.close();
  }

  getToolbarTitle(){
    return this.hasKey() ? "Modify Ingredient" : "New Ingredient";
  }

  getUpsertButtonText(){
    return this.hasKey() ? "Update" : "Add";
  }

  hasKey(){
    return this.ingredientService.form.get('id')?.value;
  }
}
