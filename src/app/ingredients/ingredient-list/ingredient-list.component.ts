import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Ingredient } from 'src/app/shared/models/ingredient';
import { IngredientService } from 'src/app/shared/services/ingredient.service';
import { IngredientComponent } from '../ingredient/ingredient.component';
import * as _ from 'lodash';
import { IngredientDatasourceService } from 'src/app/shared/services/ingredient-datasource.service';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.scss']
})
export class IngredientListComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  ingredientData!: Ingredient;
  displayedColumns: string[] = ['name', 'description', 'actions'];

  constructor(public dialog: MatDialog,
    public ingredientService: IngredientService,
    public ingredientDatasourceService: IngredientDatasourceService,
    public notificationService: NotificationService) {
    this.ingredientData = {} as Ingredient;
  }

  applyFilter(event: Event) {
    this.ingredientDatasourceService.applyFilter(event);
  }

  ngOnInit(): void {
    this.ingredientDatasourceService.init(this.sort, this.paginator);
    this.getAllIngredients();
  }

  getAllIngredients() {
    return this.ingredientDatasourceService.getAllIngredients();
  }

  editIngredient(element: Ingredient) {
    this.ingredientData = _.cloneDeep(element);
    this.ingredientService.populateForm(this.ingredientData);
    this.openIngredientDialog();
  }

  deleteIngredient(id: number) {
    this.ingredientDatasourceService.deleteIngredient(id);
    this.notificationService.onWarn(':: Deleted Successfully!');
  }

  addIngredient() {
    this.ingredientDatasourceService.addIngredient(this.ingredientData);
  }

  updateIngredient() {
    this.ingredientService.updateItem(this.ingredientData.id, this.ingredientData).subscribe((response: any) => {
      this.ingredientDatasourceService.refresh();
    });
  }

  onCreate(){
    this.ingredientService.form.reset();
    this.ingredientService.initializeFormGroup();
    this.openIngredientDialog();
  }

  openIngredientDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "25%";
    this.dialog.open(IngredientComponent, dialogConfig);
  }
}
