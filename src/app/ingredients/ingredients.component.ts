import { AfterViewInit, Component } from '@angular/core';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent implements AfterViewInit {
  title: string = "Ingredients";

  constructor() {
    
  }

  ngAfterViewInit(): void {
  }
}
