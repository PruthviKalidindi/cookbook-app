import { AfterViewInit, Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements AfterViewInit {
  title:string = "Recipes";
  constructor() {
  }

  ngAfterViewInit(): void {
  }
}
