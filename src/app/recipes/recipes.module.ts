import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { MaterialModule } from '../material/material.module';
import { RecipesComponent } from '../recipes/recipes.component';
import { RecipesRoutingModule } from './recipes-routing.module';
import { RecipeComponent } from './recipe/recipe.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [RecipeListComponent, RecipesComponent, RecipeComponent],
  imports: [
    CommonModule,
    FormsModule,
    RecipesRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [],
  entryComponents: []
})
export class RecipesModule { }
