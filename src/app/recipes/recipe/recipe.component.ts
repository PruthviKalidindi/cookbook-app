import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { RecipeDatasourceService } from 'src/app/shared/services/recipe-datasource.service';
import { RecipeService } from 'src/app/shared/services/recipe.service';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent {
  constructor(private fb: FormBuilder,
    public recipeService: RecipeService,
    public recipeDatasourceService: RecipeDatasourceService,
    public notificationService: NotificationService,
    public dialogRef: MatDialogRef<RecipeComponent>) {}

  onSubmit(): void {
    let operationMessage: string = !this.hasKey() ? "Created" : "Updated";
    if(this.recipeService.form.valid){
      if(!this.hasKey()){
        this.recipeService.createItem(this.recipeService.form.value).subscribe((response: any) => {
          this.recipeDatasourceService.refresh();
        });
      } else {
        this.recipeService.updateItem(this.recipeService.form.get('id')?.value, this.recipeService.form.value).subscribe((response: any) => {
          this.recipeDatasourceService.refresh();
        });
      }
      this.onClear();
      this.notificationService.onSuccess(`:: ${operationMessage} Successfully!`);
      this.onClose();
    }
  }

  onClear(){
    this.recipeService.form.reset();
    this.recipeService.initializeFormGroup();
    this.notificationService.onSuccess(':: Cleared Successfully!');
  }

  onClose(){
    this.dialogRef.close();
  }

  getToolbarTitle(){
    return this.hasKey() ? "Modify Recipe" : "New Recipe";
  }

  getUpsertButtonText(){
    return this.hasKey() ? "Update" : "Add";
  }

  hasKey(){
    return this.recipeService.form.get('id')?.value;
  }

  getRecipeImageUrl(){
    return this.recipeService.form.get('image')?.value;
  }
}
