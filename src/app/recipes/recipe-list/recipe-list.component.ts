import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RecipeComponent } from '../recipe/recipe.component';
import { RecipeService } from 'src/app/shared/services/recipe.service'
import { Recipe } from 'src/app/shared/models/recipe';
import { RecipeDatasourceService } from 'src/app/shared/services/recipe-datasource.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  recipeData!: Recipe;
  displayedColumns: string[] = ['title', 'description', 'cookTime', 'preparationTime', 'readyIn', 'servings', 'actions'];

  constructor(public dialog: MatDialog,
    public recipeService: RecipeService,
    public recipeDatasourceService: RecipeDatasourceService,
    public notificationService: NotificationService) {
    this.recipeData = {} as Recipe;
  }

  applyFilter(event: Event) {
    this.recipeDatasourceService.applyFilter(event);
  }

  ngOnInit(): void {
    this.recipeDatasourceService.init(this.sort, this.paginator);
    this.getAllRecipes();
  }

  getAllRecipes() {
    return this.recipeDatasourceService.getAllRecipes();
  }

  editRecipe(element: Recipe) {
    this.recipeData = _.cloneDeep(element);
    this.recipeService.populateForm(this.recipeData);
    this.openRecipeDialog();
  }

  deleteRecipe(id: number) {
    this.recipeDatasourceService.deleteRecipe(id);
    this.notificationService.onWarn(':: Deleted Successfully!');
  }

  addRecipe() {
    this.recipeDatasourceService.addRecipe(this.recipeData);
  }

  updateRecipe() {
    this.recipeService.updateItem(this.recipeData.id, this.recipeData).subscribe((response: any) => {
      this.recipeDatasourceService.refresh();
    });
  }

  onCreate(){
    this.recipeService.form.reset();
    this.recipeService.initializeFormGroup();
    this.openRecipeDialog();
  }

  openRecipeDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "50%";
    this.dialog.open(RecipeComponent, dialogConfig);
  }
}