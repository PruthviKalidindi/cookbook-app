export class Recipe {
    id!: number;
    title!: string;
    description!: string;
    preparationTimeInSeconds!: number;
    cookTimeInSeconds!: number;
    readyInSeconds!: number;
    servings!: number;
    image!: string;
}
