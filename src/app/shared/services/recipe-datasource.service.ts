import { Injectable, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Recipe } from '../models/recipe';
import { RecipeService } from './recipe.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeDatasourceService {
  recipeDataSource!:MatTableDataSource<Recipe>;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  constructor(public recipeService: RecipeService) { }

  init(sort: MatSort, paginator: MatPaginator){
    this.sort = sort;
    this.paginator = paginator;
  }

  getAllRecipes() {
    this.recipeService.getList().subscribe((response: any) => {
      this.recipeDataSource = new MatTableDataSource<Recipe>();
      this.recipeDataSource.sort = this.sort;
      this.recipeDataSource.paginator = this.paginator;
      this.recipeDataSource.data = response;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.recipeDataSource.filter = filterValue.trim().toLowerCase();
    if (this.recipeDataSource.paginator) {
      this.recipeDataSource.paginator.firstPage();
    }
  }

  deleteRecipe(id: number) {
    this.recipeService.deleteItem(id).subscribe((response: Recipe) => {
      this.refresh();
    });
  }

  addRecipe(Recipe: Recipe) {
    this.recipeService.createItem(Recipe).subscribe((response: Recipe) => {
      this.recipeDataSource.data.push({ ...response })
      this.recipeDataSource.data = this.recipeDataSource.data.map(o => {
        return o;
      })
    });
  }

  refresh() {
    this.recipeService.getList().subscribe((response: any) => {
      this.recipeDataSource.data = response;
    });
  }
}
