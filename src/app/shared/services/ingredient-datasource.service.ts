import { Injectable, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Ingredient } from '../models/ingredient';
import { IngredientService } from './ingredient.service';

@Injectable({
  providedIn: 'root'
})
export class IngredientDatasourceService {
  ingredientDataSource!:MatTableDataSource<Ingredient>;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  constructor(public ingredientService: IngredientService) { }

  init(sort: MatSort, paginator: MatPaginator){
    this.sort = sort;
    this.paginator = paginator;
  }

  getAllIngredients() {
    this.ingredientService.getList().subscribe((response: any) => {
      this.ingredientDataSource = new MatTableDataSource<Ingredient>();
      this.ingredientDataSource.sort = this.sort;
      this.ingredientDataSource.paginator = this.paginator;
      this.ingredientDataSource.data = response;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.ingredientDataSource.filter = filterValue.trim().toLowerCase();
    if (this.ingredientDataSource.paginator) {
      this.ingredientDataSource.paginator.firstPage();
    }
  }

  deleteIngredient(id: number) {
    this.ingredientService.deleteItem(id).subscribe((response: Ingredient) => {
      this.refresh();
    });
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredientService.createItem(ingredient).subscribe((response: Ingredient) => {
      this.ingredientDataSource.data.push({ ...response })
      this.ingredientDataSource.data = this.ingredientDataSource.data.map(o => {
        return o;
      })
    });
  }

  refresh() {
    this.ingredientService.getList().subscribe((response: any) => {
      this.ingredientDataSource.data = response;
    });
  }
}
