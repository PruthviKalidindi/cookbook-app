import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Ingredient } from '../models/ingredient';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  apiUrl!:string;
  ingredientsApiUrl!:string;
  
  form: FormGroup = new FormGroup({
    id: new FormControl(0),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  initializeFormGroup(){
    this.form.setValue({
      id: 0,
      name: '',
      description: ''
    });
  }
  
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
    this.ingredientsApiUrl = `${this.apiUrl}/api/Ingredients`;
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };

  // Create a new Ingredient item
  createItem(item: Ingredient): Observable<Ingredient> {
    return this.http
      .post<Ingredient>(this.ingredientsApiUrl, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get single Ingredient data by ID
  getItem(id: number): Observable<Ingredient> {
    return this.http
      .get<Ingredient>(`${this.ingredientsApiUrl}/${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get Ingredients data
  getList(): Observable<Ingredient> {
    return this.http
      .get<Ingredient>(this.ingredientsApiUrl)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Update Ingredient item by id
  updateItem(id: number, item: Ingredient): Observable<Ingredient> {
    return this.http
      .put<Ingredient>(`${this.ingredientsApiUrl}/${id}`, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Delete Ingredient item by id
  deleteItem(id: number) {
    return this.http
      .delete<Ingredient>(`${this.ingredientsApiUrl}/${id}`, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  populateForm(ingredient: Ingredient){
    this.form.setValue(ingredient);
  }
}
