import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Recipe } from '../models/recipe';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  apiUrl!:string;
  RecipesApiUrl!:string;
  
  form: FormGroup = new FormGroup({
    id: new FormControl(0),
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    preparationTimeInSeconds: new FormControl(0, Validators.required),
    cookTimeInSeconds: new FormControl(0, Validators.required),
    readyInSeconds: new FormControl(0, Validators.required),
    servings: new FormControl(0, Validators.required),
    image: new FormControl('', Validators.required)
  });

  initializeFormGroup(){
    this.form.setValue({
      id: 0,
      title: '',
      description: '',
      preparationTimeInSeconds: 0,
      cookTimeInSeconds: 0,
      readyInSeconds: 0,
      servings: 0,
      image: ''
    });
  }
  
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
    this.RecipesApiUrl = `${this.apiUrl}/api/Recipes`;
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };

  // Create a new Recipe item
  createItem(item: Recipe): Observable<Recipe> {
    return this.http
      .post<Recipe>(this.RecipesApiUrl, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get single Recipe data by ID
  getItem(id: number): Observable<Recipe> {
    return this.http
      .get<Recipe>(`${this.RecipesApiUrl}/${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get Recipes data
  getList(): Observable<Recipe> {
    return this.http
      .get<Recipe>(this.RecipesApiUrl)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Update Recipe item by id
  updateItem(id: number, item: Recipe): Observable<Recipe> {
    return this.http
      .put<Recipe>(`${this.RecipesApiUrl}/${id}`, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Delete Recipe item by id
  deleteItem(id: number) {
    return this.http
      .delete<Recipe>(`${this.RecipesApiUrl}/${id}`, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  populateForm(Recipe: Recipe){
    this.form.setValue(Recipe);
  }

  getTimeByControl(prop: string){
    if(this.form.get(prop)?.value) {
      return this.getTimeFromSeconds(this.form.get(prop)?.value);
    }
    return this.getTimeFromSeconds(0);
  }

  getTimeFromSeconds(seconds: number){
    return new Date(seconds * 1000).toISOString().substr(11, 8);
  }
}


