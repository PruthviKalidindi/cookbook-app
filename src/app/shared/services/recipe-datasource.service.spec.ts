import { TestBed } from '@angular/core/testing';

import { RecipeDatasourceService } from './recipe-datasource.service';

describe('RecipeDatasourceService', () => {
  let service: RecipeDatasourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecipeDatasourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
