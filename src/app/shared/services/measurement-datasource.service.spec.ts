import { TestBed } from '@angular/core/testing';

import { MeasurementDatasourceService } from './measurement-datasource.service';

describe('MeasurementDatasourceService', () => {
  let service: MeasurementDatasourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MeasurementDatasourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
