import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import { Measurement } from '../models/measurement';

@Injectable({
  providedIn: 'root'
})
export class MeasurementService {
  apiUrl!:string;
  measurementsApiUrl!:string;
  
  form: FormGroup = new FormGroup({
    id: new FormControl(0),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
  });

  initializeFormGroup(){
    this.form.setValue({
      id: 0,
      name: '',
      description: ''
    });
  }
  
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
    this.measurementsApiUrl = `${this.apiUrl}/api/Measurements`;
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };

  // Create a new Measurement item
  createItem(item: Measurement): Observable<Measurement> {
    return this.http
      .post<Measurement>(this.measurementsApiUrl, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get single Measurement data by ID
  getItem(id: number): Observable<Measurement> {
    return this.http
      .get<Measurement>(`${this.measurementsApiUrl}/${id}`)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get Measurements data
  getList(): Observable<Measurement> {
    return this.http
      .get<Measurement>(this.measurementsApiUrl)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Update Measurement item by id
  updateItem(id: number, item: Measurement): Observable<Measurement> {
    return this.http
      .put<Measurement>(`${this.measurementsApiUrl}/${id}`, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Delete Measurement item by id
  deleteItem(id: number) {
    return this.http
      .delete<Measurement>(`${this.measurementsApiUrl}/${id}`, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  populateForm(measurement: Measurement){
    this.form.setValue(measurement);
  }
}
