import { TestBed } from '@angular/core/testing';

import { IngredientDatasourceService } from './ingredient-datasource.service';

describe('IngredientDatasourceService', () => {
  let service: IngredientDatasourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IngredientDatasourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
