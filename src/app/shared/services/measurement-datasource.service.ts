import { Injectable, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Measurement } from '../models/measurement';
import { MeasurementService } from './measurement.service';

@Injectable({
  providedIn: 'root'
})
export class measurementDatasourceService {
  measurementDataSource!:MatTableDataSource<Measurement>;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  constructor(public measurementService: MeasurementService) { }

  init(sort: MatSort, paginator: MatPaginator){
    this.sort = sort;
    this.paginator = paginator;
  }

  getAllMeasurements() {
    this.measurementService.getList().subscribe((response: any) => {
      this.measurementDataSource = new MatTableDataSource<Measurement>();
      this.measurementDataSource.sort = this.sort;
      this.measurementDataSource.paginator = this.paginator;
      this.measurementDataSource.data = response;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.measurementDataSource.filter = filterValue.trim().toLowerCase();
    if (this.measurementDataSource.paginator) {
      this.measurementDataSource.paginator.firstPage();
    }
  }

  deleteMeasurement(id: number) {
    this.measurementService.deleteItem(id).subscribe((response: Measurement) => {
      this.refresh();
    });
  }

  addMeasurement(measurement: Measurement) {
    this.measurementService.createItem(measurement).subscribe((response: Measurement) => {
      this.measurementDataSource.data.push({ ...response })
      this.measurementDataSource.data = this.measurementDataSource.data.map(o => {
        return o;
      })
    });
  }

  refresh() {
    this.measurementService.getList().subscribe((response: any) => {
      this.measurementDataSource.data = response;
    });
  }
}
